<?php


namespace con;


class persona extends conexion
{
    public $fecha;
    public $id;
    public $sexo;
    public $edad;
    public $estado;

    public function __construct()
    {
        parent::__construct();
    }

    public function registrarpersona(){
        $pre=mysqli_prepare($this->cone,"INSERT INTO personas(edad, sexo, estado) VALUES (?,?,?)");
        $pre->bind_param("iss",$this->edad,$this->sexo,$this->estado);
        $pre->execute();
        $this->id=mysqli_insert_id($this->cone);
    }

    static function covid($idpersona){
        $cone=new conexion();
        $pre=mysqli_prepare($cone->cone, "SELECT * FROM personas WHERE id=?");
        $pre->bind_param("i",$idpersona);
        $pre->execute();
        $resultado=$pre->get_result();
        return $resultado->fetch_object();
    }
    public function registroCovid(){
        $pre=mysqli_prepare($this->cone,"INSERT INTO contagioscovid(id,edad, sexo, estado, fecha_contagio) VALUES (?,?,?,?,?)");
        $pre->bind_param("iisss",$this->id,$this->edad,$this->sexo,$this->estado,$this->fecha);
        $pre->execute();
    }
    static function infPorEstado($estadob){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas WHERE estado=?");
        $pre->bind_param("s",$estadob);
        $pre->execute();
        $resultado=$pre->get_result();
        $total=mysqli_num_rows($resultado);
        return $total;
    }

    static function mujeres($estadob){
        $con=new conexion();
        $genero="Mujer";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas WHERE estado=? AND sexo=?");
        $pre->bind_param("ss",$estadob,$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function hombres($estadob){
        $con=new conexion();
        $genero="Hombre";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas WHERE estado=? AND sexo=?");
        $pre->bind_param("ss",$estadob,$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function edadPromedio($estadob){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT SUM(edad)/COUNT(edad) FROM personas WHERE estado=?");
        $pre->bind_param("s",$estadob);
        $pre->execute();
        $suma=$pre->get_result();
        return $suma->fetch_row();
    }

    static function infPais(){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas");
        $pre->execute();
        $resultado=$pre->get_result();
        $total=mysqli_num_rows($resultado);
        return $total;
    }

    static function pmujeres(){
        $con=new conexion();
        $genero="Mujer";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas WHERE sexo=?");
        $pre->bind_param("s",$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function phombres(){
        $con=new conexion();
        $genero="Hombre";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM personas WHERE sexo=?");
        $pre->bind_param("s",$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function pedadPromedio(){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT SUM(edad)/COUNT(edad) FROM personas");
        $pre->execute();
        $suma=$pre->get_result();
        return $suma->fetch_row();
    }















    static function infPorCEstado($estadob){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT * FROM contagioscovid WHERE estado=?");
        $pre->bind_param("s",$estadob);
        $pre->execute();
        $resultado=$pre->get_result();
        $total=mysqli_num_rows($resultado);
        return $total;
    }

    static function mujeresC($estadob){
        $con=new conexion();
        $genero="Mujer";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM contagioscovid WHERE estado=? AND sexo=?");
        $pre->bind_param("ss",$estadob,$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function hombresC($estadob){
        $con=new conexion();
        $genero="Hombre";
        $pre=mysqli_prepare($con->cone,"SELECT * FROM contagioscovid WHERE estado=? AND sexo=?");
        $pre->bind_param("ss",$estadob,$genero);
        $pre->execute();
        $resultado=$pre->get_result();
        $totalmujeres=mysqli_num_rows($resultado);
        return $totalmujeres;
    }
    static function primerfecha($fechau,$fechad){
        $con=new conexion();
        $pre=mysqli_prepare($con->cone,"SELECT * FROM contagioscovid WHERE feha_contagio BETWEEN ? AND ?");
        $pre->bind_param("ss",$fechau,$fechad);
        $pre->execute();
        $resultado=$pre->get_result();
        $contagios=mysqli_num_rows($resultado);
        return $contagios;
    }
    static function segundafecha($fechat,$fechac){

    }

}