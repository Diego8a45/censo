<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pandemia/public/css/censoCovid.css">
    <title>Informacion del estado</title>
</head>
<body>

<center>
    <h1>Informaci&oacuten de estado</h1>



    <form method="post" action="index.php?controller=persona&action=mostrarEstado">
        <label for="start">Seleccione el estado del que desea conocer su informacion  </label>

        <select name="estado" id="">
            <option selected value="0"> Elige una opción </option>
            <option value="Aguascalientes">Aguascalientes</option>
            <option value="Baja california">Baja california</option>
            <option value="Baja california sur">Baja california sur</option>
            <option value="Campeche">Campeche</option>
            <option value="Chiapas">Chiapas</option>
            <option value="Chihuahua">Chihuahua</option>
            <option value="Coahuila de Zaragoza">Coahuila de Zaragoza </option>
            <option value="Colima">Colima</option>
            <option value="Durango">Durango</option>
            <option value="Estado de México">Estado de México</option>
            <option value="Guanajuato">Guanajuato</option>
            <option value="Guerrero">Guerrero</option>
            <option value="Hidalgo">Hidalgo</option>
            <option value="Jalisco">Jalisco</option>
            <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
            <option value="Morelos">Morelos</option>
            <option value="Nayarit">Nayarit</option>
            <option value="Nuevo León">Nuevo León</option>
            <option value="Oaxaca">Oaxaca</option>
            <option value="Puebla">Puebla</option>
            <option value="Queretaro">Queretaro</option>
            <option value="Quintana Roo">Quintana Roo</option>
            <option value="San Luis Potosí">San Luis Potosí</option>
            <option value="Sinaloa">Sinaloa</option>
            <option value="Sonora">Sonora</option>
            <option value="Tabasco">Tabasco</option>
            <option value="Tamaulipas">Tamaulipas</option>
            <option value="Tlaxcala">Tlaxcala</option>
            <option value="Veracruz">Veracruz </option>
            <option value="Yucatan">Yucatan</option>
            <option value="Zacatecas">Zacatecas</option>
        </select>

        <label class="pedir"> Numero total de habitantes</label>
        <input type="text" class="ingreso" value="<?php echo $_SESSION["total"]?>" disabled>
        <label class="pedir">Mujeres:</label>
        <input type="text" class="ingreso" value="<?php echo $_SESSION["mujeres"]?>" disabled>
        <label class="pedir">Hombres:</label>
        <input type="text" class="ingreso" value="<?php echo $_SESSION["hombres"]?>" disabled>
        <label class="pedir">Edad promedio:</label>
        <label class="ingreso"><?php echo $_SESSION["prom"]?></label>
        <input type="submit" value="Buscar"><br><br>

        <a href="">¿Quiere ver el incremento o decremento de contagiados ?</a>
        <br>
        <a href="index.php?controller=persona&action=inicio">Regresar</a>
    </form>
</center>

</body>
</html>