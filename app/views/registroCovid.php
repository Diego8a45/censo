<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="/pandemia/public/css/fecha.css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximun-scale=1.0, minimun-scale=1.0">
    <title>Fecha</title>
</head>
<body>
<div class="contenedor">
    <center>
        <form method="post" action="index.php?controller=persona&action=registrar">
            <label for="start">Selecciona la fecha en que se presento la enfermedad</label>
            <input type="date" id="st" name="fecha">
            <input type="submit" value="registrar" class="bot">
            <a href="index.php?controller=persona&action=inicio">Regresar</a>
        </form>
    </center>
</div>
</html>