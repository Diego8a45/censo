<?php
require 'app/modelos/conexion.php';
require 'app/modelos/persona.php';

use cone\conexion;
use con\persona;

class personaController
{
    public function inicio(){
        require 'app/views/paginaInicio.php';
    }
    public function registro(){
        require 'app/views/registroPersona.php';
        if((isset($_POST["sexo"])) || (isset($_POST["edad"])) || (isset($_POST["estado"])) ){
            $objeto=new persona();
            $objeto->sexo=$_POST["sexo"];
            $objeto->edad=$_POST["edad"];
            $objeto->estado=$_POST["estado"];
            $objeto->registrarpersona();
            echo "Su ID de registo es: $objeto->id <br>";
            echo "Importante guardar este ID para futuros procesos";
        }
    }
    public function idv(){
        require 'app/views/ingresarid.php';
    }

    function ingresaid(){

        if((isset($_POST["id"]))){
            $idPersona=$_POST["id"];
            $obj=persona::covid($idPersona);
            if($obj){
                require 'app/views/registroCovid.php';
                $_SESSION["id"]=$obj->id;
                $_SESSION["edad"]=$obj->edad;
                $_SESSION["sexo"]=$obj->sexo;
                $_SESSION["estado"]=$obj->estado;
            }
            else{
                require 'app/views/ingresarid.php';
            }
        }
        else{
            require 'app/views/ingresarid.php';
        }
    }
    public function registrar(){
        require 'app/views/registroCovid.php';
        if (isset($_POST["fecha"])){
            $object=new persona();
            $object->id=$_SESSION["id"];
            $object->edad=$_SESSION["edad"];
            $object->sexo=$_SESSION["sexo"];
            $object->estado=$_SESSION["estado"];
            $object->fecha=$_POST["fecha"];
            $object->registroCovid();
            echo "Registro guardado correctamente";
        }
}
    public function mostrarEstado(){
        if((isset($_POST["estado"]))){
            $estadob=$_POST["estado"];
            $total=$_SESSION["total"];
            $obje=persona::infPorEstado($estadob);
            $obje1=persona::mujeres($estadob);
            $obje2=persona::hombres($estadob);
            $obje4=persona::edadPromedio($estadob);
            if ($obje){
                $_SESSION["estado"]=$estadob;
                $_SESSION["total"]=$obje;
                $_SESSION["mujeres"]=$obje1;
                $_SESSION["hombres"]=$obje2;
                $_SESSION["prom"]=json_encode($obje4);
                require 'app/views/censoEstado.php';
            }
            else{
                $_SESSION["estado"]="";
                $_SESSION["total"]="";
                $_SESSION["mujeres"]="";
                $_SESSION["hombres"]="";
                $_SESSION["prom"]="";
                require 'app/views/censoEstado.php';
                echo "no hay habitantes registrados por ahora";
            }
        }
        else{
            require 'app/views/censoEstado.php';
        }

}
    public function mostrarPais(){
        $total=$_SESSION["total"];
        $obje=persona::infPais();
        $obje1=persona::pmujeres();
        $obje2=persona::phombres();
        $obje4=persona::pedadPromedio();
        if ($obje){
            $_SESSION["total"]=$obje;
             $_SESSION["mujeres"]=$obje1;
            $_SESSION["hombres"]=$obje2;
            $_SESSION["prom"]=json_encode($obje4);
            require 'app/views/censoPais.php';
        }
        else{
            echo "no hay";
        }
    }
    public function mostrarContagiosEstado()
    {
        if ((isset($_POST["estado"]))) {
            $estadob = $_POST["estado"];
            $total = $_SESSION["total"];
            $obje = persona::infPorCEstado($estadob);
            $obje1 = persona::mujeresc($estadob);
            $obje2 = persona::hombresc($estadob);
            $obje4 = persona::edadPromedio($estadob);
            if ($obje) {
                $_SESSION["estado"] = $estadob;
                $_SESSION["total"] = $obje;
                $_SESSION["mujeres"] = $obje1;
                $_SESSION["hombres"] = $obje2;
                $_SESSION["prom"] = json_encode($obje4);
                require 'app/views/censoCovid.php';
            } else {
                $_SESSION["estado"]="";
                $_SESSION["total"]="";
                $_SESSION["mujeres"]="";
                $_SESSION["hombres"]="";
                $_SESSION["prom"]="";
                require 'app/views/censoCovid.php';
                echo "no hay habitantes contagiados registrados por ahora";
            }
        } else {
            require 'app/views/censoCovid.php';
        }
    }

    public function cifras(){
        require 'app/views/cifras.php';
        $fechau=$_POST["fechaps"];
        $fechad=$_POST["fechapss"];
        $fechat=$_POST["fechass"];
        $fechac=$_POST["fechasss"];
        $funcion=persona::primerfecha($fechau,$fechad);
        $funcio=persona::segundafecha($fechat,$fechac);
        if($funcion){
            echo json_encode($funcion);

        }

    }

}